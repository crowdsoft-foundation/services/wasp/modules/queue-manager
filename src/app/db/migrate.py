from db.abstract import Migrate
from sqlalchemy import types


def migrate_db():
    migration = Migrate()

    migration.add_column(table_name="queue_entries", column_name="links", data_type=types.TEXT())
    default_links = '[]'
    migration.execute("Set default links", f"UPDATE `queue_entries` set links = '{default_links}' where links is null;")
    migration.execute("Set entry_id unique to entry_id+owner_id", f"ALTER TABLE `queue_entries` DROP INDEX `entry_id`, ADD UNIQUE INDEX `entry_id` (`entry_id`, `owner_id`) USING BTREE;")
    migration.execute("Drop wrongly named database taskmanager_template_groups", f"DROP TABLE IF EXISTS `taskmanager_template_groups`;")
    migration.execute("Rename column cron_expression to scheduling_interval", f"ALTER TABLE `template_groups` CHANGE COLUMN `cron_expression` `scheduling_interval` BIGINT UNSIGNED;")
    migration.add_column(table_name="template_groups", column_name="start_date", data_type=types.DATETIME())
    migration.add_column(table_name="template_groups", column_name="last_run", data_type=types.DATETIME())
    migration.add_column(table_name="template_groups", column_name="run_counter", data_type=types.BIGINT(), default=0)

