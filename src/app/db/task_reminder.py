from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence, Boolean, BigInteger
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base
import copy
import datetime
import json


Base = declarative_base()
config = Config()


class TaskReminder(DbEntity, Base):
    __tablename__ = 'task_reminders'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_general_ci'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    reminder_id = Column(String(255))
    owner = Column(String(255))
    creator = Column(String(255))
    task_id = Column(String(255))
    seconds_in_advance = Column(BigInteger)
    type = String(255)
    definition = Column(Text)
    creation_time = Column(DateTime,  default=datetime.datetime.utcnow)
    processed_time = Column(DateTime, default=None)
    data_owners = Column(Text, default='[]')

    def __init__(self, create_table_if_not_exists=False):
        super().__init__()
        if create_table_if_not_exists:
            Base.metadata.create_all(Config.db_engine, checkfirst=True)

    def __repr__(self):
        return "<TaskReminder(owner='%s', creator='%s', reminder_id='%s', definition='%s', data_owners='%s')>" % (
            self.owner, self.creator, self.reminder_id, self.definition, self.data_owners)

    def save(self):
        self.fire()

    def commit(self):
        raise Exception("For events, usage of commit and add functions are forbidden. Either use save() or fire()")

    def add(self):
        raise Exception("For events, usage of commit and add functions are forbidden. Either use save() or fire()")

    def rollback(self):
        raise Exception("For events, usage of rollback is forbidden.")

    def fire(self):
        message = copy.deepcopy(self.__dict__)
        if "_sa_instance_state" in message:
            del message["_sa_instance_state"]

        try:
            self.session.add(self)
            self.session.flush()
        except Exception as e:
            self.session.rollback()
            raise



from sqlalchemy.ext.declarative import DeclarativeMeta
class AlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields

        return json.JSONEncoder.default(self, obj)