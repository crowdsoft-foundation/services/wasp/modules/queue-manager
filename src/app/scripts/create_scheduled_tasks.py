import sys
sys.path.append("/src/app")
from config import Config

import datetime
import pytz
import json
from pbglobal.commands.createNewQueueEntry import createNewQueueEntry
from pbglobal.pblib.amqp import client
from pbglobal.pblib.pbdatecalc import PbDateCalc

current_timezone = pytz.timezone("Europe/Berlin")
current_time = datetime.datetime.now(pytz.timezone('Europe/Berlin'))
sql = "SELECT * FROM template_groups where scheduling_interval is not null and start_date < :current_time"
results = Config.db_session.execute(sql, {"current_time": current_time}).fetchall()

for row in results:
    if row.scheduling_interval is None:
        continue
    new_run_counter = row.run_counter + 1
    to_be_scheduled_template_list = json.loads(row.data).get("templates")

    for template in to_be_scheduled_template_list:
        queue_id = template.get("queue_id", "")
        for template_entry in template.get("template_ids", ""):
            template_id = template_entry.get("id")

            command_creation_time = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
            calculated_next_run_timestamp = row.run_counter * row.scheduling_interval + row.start_date.timestamp()
            if calculated_next_run_timestamp <= current_time.timestamp():
                sql = "SELECT * FROM task_templates where template_id = :template_id"
                task_template_db = Config.db_session.execute(sql,  {"template_id": template_id}).first()

                if task_template_db is None:
                    continue

                sent_command = createNewQueueEntry(skip_validation=True)

                sent_command.owner = task_template_db.owner_id
                sent_command.creator = task_template_db.creator

                sent_command.correlation_id = f"scheduled_{row.group_id}_{template_id}_{calculated_next_run_timestamp}"
                sent_command.create_time = command_creation_time
                sent_command.owner_id = task_template_db.owner_id
                data = json.loads(task_template_db.data)
                sent_command.links = data.get("links", [])
                del(data["links"])
                data["generatedBy"] = f"Scheduler_{row.group_id}_{template_id}_{calculated_next_run_timestamp}"
                if type(data["due_date"]) == dict:
                    data["due_date"] = PbDateCalc(data["due_date"]).get_calculated_date().strftime('%Y-%m-%dT%H:%M:%S.000Z')
                sent_command.entry_data = data

                sent_command.data_owners = json.loads(task_template_db.data_owners)

                if sent_command.validate():
                    sent_command.amqp_client = client
                    sent_command.publish()
                    sql = "UPDATE template_groups SET run_counter=:run_counter, last_run=now() WHERE group_id = :group_id"
                    results = Config.db_session.execute(sql, {"group_id": row.group_id, "run_counter": new_run_counter})
                else:
                    print(f"ERROR in scheduled task creation for template_group {template_id} in template-group: {row.group_id}")



