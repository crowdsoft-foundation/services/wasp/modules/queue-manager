##
import requests
import json
import os
from db.queue import Queue
from db.queue_entry import QueueEntry
from db.task_template import TaskTemplate
from db.task_reminder import TaskReminder
from db.resource import Resource
from db.template_group import TemplateGroup
from db.migrate import migrate_db

class AutorunBasic:

    def __init__(self):
        self.ENVIRONMENT = os.getenv("ENVIRONMENT")
        self.kong_api = os.getenv("APIGATEWAY_ADMIN_INTERNAL_BASEURL")
        print("Detected environment:", self.ENVIRONMENT)
        print("Using " + str(self.kong_api) + "as api-gateway")

    def post_deployment(self):
        pass

    def add_service(self, service_name, service_url):
        payload = {
            "name": service_name,
            "url": service_url
        }
        headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache",
        }
        url = self.kong_api + "/services"
        response = requests.request("GET", url, headers=headers)

        services = response.json()
        for service in services.get("data", []):
            if service_name == service.get("name"):
                print("Service already exists")
                return

        self.post(api_path="/services", payload=payload, headers=headers)

    def add_route(self, service_name, path, plugins=["key-auth"], strip_path=True):
        payload = {
            "paths": [path],
            "strip_path": strip_path,
        }
        headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache",
        }

        url = self.kong_api + "/services/" + service_name + "/routes/"

        response = requests.request("GET", url, headers=headers)

        routes = response.json()

        route_id = None
        path_exists = False
        for route in routes.get("data", []):
            if path in route.get("paths"):
                print("Path already exists")
                route_id = route.get("id")
                path_exists = True


        if path_exists is False:
            reply = self.post(api_path="/services/" + service_name + "/routes", payload=payload, headers=headers)

            route_id = reply.get("id")

        for plugin in plugins:
            if type(plugin) == str:
                url = self.kong_api + "/routes/" + route_id + "/plugins"
                payload = {"name": plugin}
                headers = {
                    'Content-Type': "application/json",
                    'cache-control': "no-cache",
                }

            if type(plugin) == dict:
                if plugin.get("type", "") == "acl":
                    url = self.kong_api + "/routes/" + str(route_id) + "/plugins"
                    payload = {
                        "name": "acl",
                        "config": {
                            "allow": plugin.get("allow")
                        }
                    }
                    print("PAYLOAD", payload)
                    headers = {
                        'Content-Type': "application/json",
                        'cache-control': "no-cache",
                    }
                    print("ADDING PLUGIN TO", url, payload)

            try:
                response = requests.request("POST", url, data=json.dumps(payload), headers=headers)

                if response.status_code == 409:
                    print("Plugin entry already exists")
                else:
                    print(response.text)

            except Exception:
                pass

        return route_id

    def post(self, api_path, payload, headers):
        if type(payload) == dict:
            payload = json.dumps(payload)

        url = self.kong_api + api_path
        response = requests.request("POST", url, data=payload, headers=headers)
        print(response.text)
        return response.json()



class Autorun(AutorunBasic):

    def __init__(self):
        super(Autorun, self).__init__()

    def post_deployment(self):
        Queue(create_table_if_not_exists=True)
        QueueEntry(create_table_if_not_exists=True)
        TaskTemplate(create_table_if_not_exists=True)
        TaskReminder(create_table_if_not_exists=True)
        Resource(create_table_if_not_exists=True)
        TemplateGroup(create_table_if_not_exists=True)
        migrate_db()
        print("Executing initialisation-script")
        print(self.kong_api)
        if self.kong_api == "":
            return

        request = requests.get(self.kong_api)

        if request.status_code == 200:
            self.add_service("queue-manager", "http://queue-manager.planblick.svc:8000")
            self.add_route(service_name="queue-manager", path="/get_queue_entries", strip_path=False)
            self.add_route(service_name="queue-manager", path="/queues/list", strip_path=False)
            self.add_route(service_name="queue-manager", path="/task_templates/list_by_queue", strip_path=False)
            self.add_route(service_name="queue-manager", path="/task_template/by_id", strip_path=False)
            self.add_route(service_name="queue-manager", path="/tasks/by_id", strip_path=False)
            self.add_route(service_name="queue-manager", path="/get_taskmanager_resources", strip_path=False)
            self.add_route(service_name="queue-manager", path="/get_taskmanager_template_groups", strip_path=False)
            self.add_route(service_name="queue-manager", path="/create_tasks_of_template_group", strip_path=False)
        else:
            print("Kong-API not available:", self.kong_api)

