from config import Config
from db.task_reminder import TaskReminder
from db.queue_entry import QueueEntry
from models.mail_send import send_single_task_reminder_mail
import json
import os
import requests

class Reminder:
    def __init__(self, reminder_id, queue_client):
        self.data = Config.db_session.query(TaskReminder).filter(TaskReminder.reminder_id == reminder_id).one_or_none()
        self.queue_client = queue_client

    def get_group_members(self, group):
        url = os.getenv("ACCOUNTMANAGER_STORE_BASE_URL") + f"/get_group_members/{group}"

        payload = {}
        headers = {}

        response = requests.request("GET", url, headers=headers, data=payload)

        return response.json().get("members")

    def get_email_by_login(self, login):
        url = os.getenv("ACCOUNTMANAGER_STORE_BASE_URL") + "/get_login_profile?login=" + login

        payload = {}
        headers = {}

        response_json = requests.request("GET", url, headers=headers, data=payload).json()
        email = response_json.get("email")
        return email

    def sendReminder(self):
        result = False
        if self.data is not None:
            reminder_definition = json.loads(self.data.definition)
            reminder_type = reminder_definition.get("reminder_type")
            if reminder_type == "gui":
                message = {"event": "showTaskReminder",
                           "args": {"task_id": self.data.task_id}}

                for recipient in reminder_definition.get("recipients"):
                    scope, value =recipient.split(".")
                    if scope == "group":
                        for group_member in self.get_group_members(value):
                            payload = {"room": group_member.get("username"), "data": message}
                            self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                                      message=json.dumps(payload))

                    if scope == "user":
                        payload = {"room": value, "data": message}
                        self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                                  message=json.dumps(payload))

                result = True
            elif reminder_type == "email":
                task = Config.db_session.query(QueueEntry).filter(QueueEntry.entry_id == self.data.task_id).one_or_none()
                if task is not None:
                    task_payload = json.loads(task.data)

                    for recipient in reminder_definition.get("recipients"):
                        scope, value = recipient.split(".")
                        if scope == "group":
                            for group_member in self.get_group_members(value):
                                email = self.get_email_by_login(group_member.get("username"))
                                if email is not None:
                                    send_single_task_reminder_mail(recipient=email, task_payload=task_payload)

                        if scope == "user":
                            email = self.get_email_by_login(value)
                            if email is not None:
                                send_single_task_reminder_mail(recipient=email, task_payload=task_payload)
                result = True

        return result
